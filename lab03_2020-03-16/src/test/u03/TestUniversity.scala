import University._
import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test

class TestUniversity {

    import u03.Lists.List._

    val lst: Cons[Person] = Cons (
        Person.Student (" mario ", 2015),
        Cons( Person.Student (" mario1 ", 2015),
            Cons( Person.Teacher (" teacher1 ", "course 1"),
                Cons( Person.Teacher (" teacher1 ", "course 2"),
                        Cons( Person.Teacher (" teacher3 ", "course 4"), Nil() ) ) ) ) )

    @Test def TestGetCourses( ): Unit = {
        assertEquals( Nil(), getCourses( Nil() ) )
        assertEquals( Cons( "course 1", Cons( "course 2", Cons( "course 4", Nil() ) ) ), getCourses( lst ) )
    }
}

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test
import u03.Streams

class TestStream {

    import Streams.Stream
    import u03.Lists.List._

    val lst = Stream.take( Stream.iterate( 0 ) ( _ + 1 ) ) (10)

    @Test def TestDrop( ): Unit = {
        assertEquals( Stream.toList( lst ), Stream.toList( Stream.drop ( lst ) (0) ) )
        assertEquals( Cons( 6, Cons( 7, Cons( 8, Cons( 9, Nil( ) ) ) ) ), Stream.toList( Stream.drop ( lst ) (6) ) )
    }

    @Test def TestInfiniteConstantStream( ): Unit = {
        assertEquals( Cons( 3, Nil () ), Stream.toList( Stream.take ( Stream.constant( 3 ) ) ( 1 ) ) )
        assertEquals( Cons( ( 3, 7 ), Nil () ), Stream.toList( Stream.take ( Stream.constant( ( 3, 7 ) ) ) ( 1 ) ) )
        assertEquals( Cons( "x", Cons( "x", Cons( "x", Cons( "x", Cons( "x", Nil () ) ) ) ) ),
            Stream.toList( Stream.take ( Stream.constant( "x" ) ) ( 5 ) ) )
    }

    @Test def TestFibonacciStream( ): Unit = {
        assertEquals( Cons( 0, Nil () ), Stream.toList( Stream.take ( Stream.fibs ) ( 1 ) ) )
        assertEquals( Cons( 0, Cons( 1, Nil() ) ), Stream.toList( Stream.take ( Stream.fibs ) ( 2 ) ) )
        assertEquals( Cons( 0, Cons( 1, Cons( 1, Cons( 2, Cons( 3, Nil() ) ) ) ) ),
            Stream.toList( Stream.take ( Stream.fibs ) ( 5 ) ) )
        assertEquals( Cons( 0, Cons( 1, Cons( 1, Cons( 2, Cons( 3, Cons( 5, Cons( 8, Cons( 13, Nil () ) ) ) ) ) ) ) ),
            Stream.toList( Stream.take ( Stream.fibs ) ( 8 ) ) )
    }
}

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test

class TestLists {

    import u03.Lists.List._

    val lst: Cons[Int] = Cons (10 , Cons (20 , Cons (30 , Nil () )) )

    @Test def TestDrop( ): Unit = {
        assertEquals( Cons( 20 , Cons ( 30 , Nil( ) ) ), drop( lst, 1 ) )
        assertEquals( Cons ( 30 , Nil( ) ), drop( lst, 2 ) )
        assertEquals( Nil ( ), drop( lst, 5 ) )
    }

    @Test def TestAppend( ): Unit = {
        assertEquals( append( Nil(), Nil() ), Nil() )
        assertEquals( append( Cons( 40, Nil( ) ), Nil() ), Cons( 40, Nil() ) )
        assertEquals( append( Cons( 40, Nil( ) ), Cons( 50, Nil( ) ) ), Cons( 40, Cons (50 , Nil() ) ) )
    }

    @Test def TestFlatMap( ): Unit = {
        assertEquals( flatMap ( lst )(v => Cons ( v + 1, Nil() ) ), Cons (11 , Cons (21 , Cons (31 , Nil ()))) )
        assertEquals( flatMap ( lst )(v => Cons ( v + 1, Cons(v +2 , Nil () ) ) ), Cons (11 , Cons (12 , Cons (21 , Cons (22 , Cons (31 , Cons (32 , Nil ())))))) )
    }

    @Test def TestMap( ): Unit = {
        assertEquals( map ( lst )(v => "str " + v), Cons ( "str 10" , Cons ( "str 20" , Cons ( "str 30" , Nil ()))) )
    }

    @Test def TestFilter( ): Unit = {
        assertEquals( filter ( lst )(v => v < 30 ), Cons ( 10 , Cons ( 20 , Nil ())) )
    }

    @Test def TestMax( ): Unit = {
        assertEquals( max( 2, Option(1) ), 2 )
        assertEquals( max( 1, None ), 1 )
    }

    @Test def TestMaxList( ): Unit = {
        assertEquals( max ( lst ), Option( 30 ) )
        assertEquals( max ( Nil() ), None )
    }

    @Test def TestFolds( ): Unit = {
        val lst = Cons (3 , Cons (7 , Cons (1 , Cons (5 , Nil ( ) ) ) ) )
        assertEquals( -16, foldLeft[Int]( lst ) (0) (_-_) )
        assertEquals( -8, foldRight[Int]( lst ) (0) (_-_) )

        val doubleLst = Cons ( 3.0, Cons ( 7.0, Cons ( 1.0, Cons ( 5.0, Nil ( ) ) ) ) )

        assertEquals( 200.0/21, foldLeft( doubleLst ) (1000.0) (_/_), 0.01 )
        assertEquals( 210, foldRight( doubleLst ) (2.0) (_*_) )

        val stringLst = Cons ( "Corso", Cons ( "paradigmi", Cons ( "programmazione", Cons ( "sviluppo", Nil ( ) ) ) ) )

        assertEquals( "Corso paradigmi programmazione sviluppo !", foldRight( stringLst ) ("!") (_+ " " +_) )
        assertEquals( "! Corso paradigmi programmazione sviluppo", foldLeft( stringLst ) ("!") (_+ " " +_) )
    }
}

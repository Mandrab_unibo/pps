package u03

object Lists {

	sealed trait List[E]

	object List {
		case class Cons[E](head: E, tail: List[E]) extends List[E]
		case class Nil[E]() extends List[E]

		def sum(l: List[Int]): Int = l match {
			case Cons(h, t) => h + sum(t)
			case _ => 0
		}

		def map[A,B](l: List[A])(mapper: A=>B): List[B] = flatMap( l )( v => Cons( mapper(v), Nil() ) )

		def filter[A](l: List[A])(pred: A=>Boolean): List[A] = flatMap(l)(v => pred( v ) match {
			case true => Cons( v, Nil() )
			case false => Nil()
		} )

		@scala.annotation.tailrec
		def drop[A](l: List[A], i: Int): List[A] = ( l, i ) match {
			case ( Nil(), _ ) | ( _, 0 ) => l
			case ( Cons( _, t ), i ) => drop( t, i - 1 )
		}

		def flatMap[A,B](l: List[A])(f: A => List[B]): List[B] = l match {
			case Nil() => Nil()
			case Cons( h, t ) => append( f( h ), flatMap( t )( f ) )
		}

		def append[A](l1: List[A], l2: List[A]): List[A] = l1 match {
			case Cons( h, t ) => Cons( h, append( t, l2 ) )
			case Nil() => l2
		}

		def max( l: List[Int] ): Option[Int] = l match {
			case Cons( h, t ) => Option( max( h, max( t ) ) )
			case Nil( ) => None
		}

		def max(i1: Int, i2: Option[Int] ): Int = i2.getOrElse( i1 ) > i1 match {
			case true => i2.get
			case false => i1
		}

		@scala.annotation.tailrec
		def foldLeft[A](l: Lists.List[A] )(f: A )(op: ( A, A ) => A ): A = l match {
			case Cons( h, t ) => foldLeft( t )( op( f, h ) )( op )
			case Nil() => f
		}

		def foldRight[A]( l: Lists.List[A] )( f: A )( op: ( A, A ) => A ): A = l match {
			case Cons( h, t ) => op( h, foldRight( t )( f )( op ) )
			case Nil() => f
		}
	}
}

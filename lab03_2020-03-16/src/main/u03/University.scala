import Person.Teacher
import u03.Lists
import u03.Lists.List.{Cons, Nil, flatMap}

object University {

    def getCourses( l : Lists.List[Person] ): Lists.List[String] = flatMap(l) {
        case p: Teacher => Cons(p.course, Nil())
        case _ => Nil()
    }
}
